# 아침식단 추천 APP

***

#### LANGUAGE
```
Dart 
Flutter
```
#### 기능
```

* 아침식사 메뉴추천
  - 든든한식단 추천
  - 간단한 과일 추천
  - 상세페이지 구현
  - 음식에 영양성분과 칼로리 보여주기

```


# app 화면

### 메인 상단
![Breakfast](./images/breakfast.png)

### 통계자료 및 추천메뉴
![Breakfast](./images/breakfast1.png)

### 통계자료 및 추천 과일
![Breakfast](./images/breakfast2.png)

### 상세페이지
![Breakfast](./images/breakfast3.png)



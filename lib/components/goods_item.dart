import 'package:flutter/material.dart';

class GoodsItem extends StatelessWidget {
  const GoodsItem({super.key, required this.photoUrl, required this.foodName, required this.content, required this.callback});

  final String photoUrl;
  final String foodName;
  final String content;
  final VoidCallback callback;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Container(
        child: Column(
          children: [
            Container(
              decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                    color: Colors.white,
                    blurRadius: 20,
                    spreadRadius: 2,
                  ),
                ],
              ),
              padding: EdgeInsets.all(10),
              child: Image.asset(
                  photoUrl,
                fit: BoxFit.fill,
                width: 110,
              ),
            ),
            Container(
              child: Text(
                foodName,
                style: TextStyle(
                  fontFamily: 'maple',
                  fontSize: 20,
                ),
              ),
            ),
            Container(
              child: Text(
                content,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ],
        ),
      ),
      onTap: callback,
    );
  }
}

import 'package:app_breakfast/components/goods_info_box.dart';
import 'package:app_breakfast/model/goods.dart';
import 'package:flutter/material.dart';

class PageGoodsDetail extends StatefulWidget {
  const PageGoodsDetail({super.key, required this.goods});

  final Goods goods;

  @override
  State<PageGoodsDetail> createState() => _PageGoodsDetailState();
}

class _PageGoodsDetailState extends State<PageGoodsDetail> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('아침메뉴 상세 페이지'),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Image.asset(widget.goods.photoUrl),
            Container(
              child: Column(
                children: [
                  Text(
                    widget.goods.foodName,
                    style: TextStyle(
                      fontFamily: 'maple',
                      fontSize: 20,
                    ),
                  ),
                  Text(
                    widget.goods.content,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Divider(
                    color: Colors.black,
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  GoodsInfoBox(title: '영양성분', contents: widget.goods.Nutrition),
                  SizedBox(
                    height: 30,
                  ),
                  GoodsInfoBox(title: '칼로리', contents: widget.goods.calorie),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

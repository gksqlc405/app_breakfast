import 'package:flutter/material.dart';

class GoodsInfoBox extends StatelessWidget {
  const GoodsInfoBox({super.key, required this.title, required this.contents});

  final String title;
  final String contents;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Text(
              title,
            style: TextStyle(fontFamily: 'maple',
            fontSize: 20,
            ),
          ),

          Text(
            contents,
            style: TextStyle(fontWeight:
            FontWeight.bold,
            ),
          ),
        ],
      ),
    );
  }
}

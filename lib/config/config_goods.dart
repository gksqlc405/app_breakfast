
import 'package:app_breakfast/model/goods.dart';

final List<Goods> configGoodsMookup = [
  Goods('assets/돼지국밥.jpeg', '돼지국밥', '아침부터 든든~하게', '지방: 10.41g | 탄수화물: 23.46g | 단백질: 27.58g', '칼로리: 307kcal'),
  Goods('assets/라면.png', '라면', '얼큰한게 짱이지', '지방: 14.54g | 탄수화물: 55.68g | 단백질: 7.90g', '칼로리: 385kcal'),
  Goods('assets/모닝빵.png', '모닝빵', '가벼운 식사', '지방: 1.43g | 탄수화물: 17.42g | 단백질: 3.28g', '칼로리: 칼로리: 97kcal'),
  Goods('assets/순대국밥.jpeg', '순대국밥', '역시 든든한 순대국밥', '지방: 9.96g | 탄수화물: 42.61g | 단백질: 23.09g', ' 칼로리: 360kcal'),
  Goods('assets/토스트.jpeg', '토스트', '뉴요커가 된기분', '지방: 0.88g | 탄수화물: 11.97g | 단백질: 1.98g', '칼로리: 64kcal'),

];

final List<Goods> configFruit = [
  Goods('assets/고구마.jpeg', '고구마', '목메여 죽는다는 고구마', '지방: 10.41g | 탄수화물: 23.46g | 단백질: 27.58g', '칼로리: 307kcal'),
  Goods('assets/바나나.jpeg', '바나나', '원숭이 원픽', '지방: 14.54g | 탄수화물: 55.68g | 단백질: 7.90g', '칼로리: 385kcal'),
  Goods('assets/방울토마토.png', '방울토마토', '방울방울방울', '지방: 1.43g | 탄수화물: 17.42g | 단백질: 3.28g', '칼로리: 칼로리: 97kcal'),
  Goods('assets/사과.jpeg', '사과', '스미마셍', '지방: 9.96g | 탄수화물: 42.61g | 단백질: 23.09g', ' 칼로리: 360kcal'),
  Goods('assets/샐러드.jpeg', '샐러드', '장식용 음식', '지방: 0.88g | 탄수화물: 11.97g | 단백질: 1.98g', '칼로리: 64kcal'),
];
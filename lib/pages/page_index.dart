
import 'package:app_breakfast/components/goods_item.dart';
import 'package:app_breakfast/components/main_title.dart';
import 'package:app_breakfast/config/config_goods.dart';
import 'package:app_breakfast/model/goods.dart';
import 'package:app_breakfast/pages/page_goods_detail.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';


class PageIndex extends StatefulWidget {
  const PageIndex({Key? key}) : super(key: key);

  @override
  State<PageIndex> createState() => _PageIndexState();
}


class _PageIndexState extends State<PageIndex> {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp
    ]);

    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Image.asset(
              'assets/메인.jpeg'
                ,
            ),
            SizedBox(
              height: 30,
            ),
            Container(
              decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                    color: Color.fromRGBO(213, 245, 10, 30),
                    blurRadius: 20,
                    spreadRadius: 2,
                  ),
                ],
                borderRadius: BorderRadius.all(
                  Radius.circular(70),
                ),
              ),
              padding: EdgeInsets.all(10),
              child:
              MainTitle(title:
              '통계 자료!',
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(
                  Radius.circular(70),
                ),
                border: Border.all(
                  color: Color.fromRGBO(213, 245, 10, 30),
                  width: 3,
                ),
              ),
              padding: EdgeInsets.all(20),
              margin: EdgeInsets.all(10),
              child: Image.asset(
                'assets/메인메뉴.png',
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              padding: EdgeInsets.all(10),
              decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                    color: Color.fromRGBO(213, 245, 10, 30),
                    blurRadius: 20,
                    spreadRadius: 2,
                  ),
                ],
                borderRadius: BorderRadius.all(
                  Radius.circular(70),
                ),
              ),
              child:  MainTitle(
                title:
                '든든~한 아침',
              ),
            ),
            SizedBox(
              height: 10,
            ),
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                children: [
                  for (int i = 0; i < configGoodsMookup.length; i++)
                    GoodsItem(
                        photoUrl: configGoodsMookup[i].photoUrl,
                        foodName: configGoodsMookup[i].foodName,
                        content: configGoodsMookup[i].content,
                        callback: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => PageGoodsDetail(
                                goods: configGoodsMookup[i],
                              ),
                            ),
                          );
                        }
                    ),
                ],
              ),
            ),
            SizedBox(
              height: 30,
            ),
            Container(
              decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                    color: Color.fromRGBO(213, 245, 10, 30),
                    blurRadius: 20,
                    spreadRadius: 2,
                  ),
                ],
                borderRadius: BorderRadius.all(
                  Radius.circular(70),
                ),
              ),
              padding: EdgeInsets.all(10),
              child:
              MainTitle(title:
              '통계 자료!',
              ),
            ),
            Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(
                  Radius.circular(70),
                ),
                border: Border.all(
                  color: Color.fromRGBO(213, 245, 10, 30),
                  width: 3,
                ),
              ),
              padding: EdgeInsets.all(20),
              margin: EdgeInsets.all(10),
              child: Image.asset(
                'assets/과일.jpeg',
              ),
            ),
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                children: [
                  for (int i = 0; i < configFruit.length; i++)
                    GoodsItem(
                        photoUrl: configFruit[i].photoUrl,
                        foodName: configFruit[i].foodName,
                        content: configFruit[i].content,
                        callback: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => PageGoodsDetail(
                                goods: configFruit[i],
                              ),
                            ),
                          );
                        }
                    ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

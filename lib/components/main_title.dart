import 'package:flutter/material.dart';

class MainTitle extends StatelessWidget {
  final String title;
  const MainTitle({super.key, required this.title}) ;



  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Text(title,
            style: TextStyle(fontFamily:
            'maple',
                fontSize: 20),
          ),
        ],
      ),
    );
  }
}
